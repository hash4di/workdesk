# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :string(255)
#  status      :boolean
#

require 'spec_helper'

describe Project do

  let (:new_project) { create(:project) }

  subject { new_project }

  context "when saved successful" do
    it { expect be_valid }
    specify { new_project.save == true }
  end
end
