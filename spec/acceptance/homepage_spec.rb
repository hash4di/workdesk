# coding: utf-8
require File.expand_path(File.dirname(__FILE__) + '/acceptance_helper')

feature "Homepage:" do

  context "Review the homepage:" do

    background do
      @project = FactoryGirl.create(:project)
    end

    scenario "enter the homepage", js: true do
      visit homepage

      expect(page).to have_text("Workdesk")
    end

    scenario "expect to have Projects column on left", js: true do
      visit homepage

      expect(page).to have_xpath("//h2[contains(.,'Projects:')]")
    end

    scenario "expect to have 'New Projec' link next to Project header on left column", :js => true do
      visit homepage

      expect(page).to have_xpath("//h2/a[contains(.,'New Project')][@href='/new']")
    end

    scenario "expect to have example project button in left column under Project header", :js => true do
      visit homepage

      expect(page).to have_xpath("//div[@class='container-fluid']/button[contains(.,'ProjectNr')]")
    end

    scenario "New Project link should open /new path", js: true do
      visit homepage
      click_link('New Project')

      expect(page).to have_xpath("//div[@id='projects']/div/h2[contains(.,'New Project')]")
      expect(page).to have_xpath("//form/div[@class='form-group']/label[contains(.,'Title:')]")
      expect(page).to have_xpath("//form/div[@class='form-group']/input[@placeholder='Enter title']")
      expect(page).to have_xpath("//form/div[@class='form-group']/label[contains(.,'Description:')]")
      expect(page).to have_xpath("//form/div[@class='form-group']/input[@placeholder='Enter description']")
      expect(page).to have_xpath("//form/div[@class='actions']/button[@type='submit'][@value='Create Project']")
    end

    scenario "Create new Project", js: true do
      visit homepage
      click_link('New Project')

      page.find(:xpath, ".//input[@placeholder='Enter title']").set("Project test title")
      page.find(:xpath, ".//input[@placeholder='Enter description']").set("Initial project description for capybara test")
      click_button('Create Project')

      expect(page).to have_text("Project test title | Initial project description for capybara test")
    end
  end

end
