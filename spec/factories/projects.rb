FactoryGirl.define do
  sequence :title do |n|
    "ProjectNr#{n} "
  end
  factory :project do
    title
    description "bar"
  end
end
