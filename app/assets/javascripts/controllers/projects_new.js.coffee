App.ProjectsNewController = Ember.Controller.extend

  actions:

    createProject: ->
      project = @store.createRecord 'project', @get('fields')
      project.save().then =>
        @transitionToRoute 'project', project
